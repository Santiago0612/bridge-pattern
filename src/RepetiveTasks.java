import java.util.ArrayList;

public class RepetiveTasks implements ImplementsList {
    private ArrayList<String> Itemslist = new ArrayList<String>();
//Esto hace parte de la implementacion
    @Override
    public void addItems(String items){
        Itemslist.add(items);
    }
    @Override
    public void  removeItem(String items){
        if(Itemslist.contains(items)){
            Itemslist.remove(Itemslist.indexOf(items));
        }
    }

    @Override
    public int quantityItem(){
        return Itemslist.size();
    }
    @Override
    public String getItem(int i){
        if (i < Itemslist.size()){
            return (String) Itemslist.get(i);
        }
        return null;
    }
    public boolean Support(){
        return true;
    }
}

