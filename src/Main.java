import java.util.Scanner;

public class Main {



    //En este patron se ahorra codigo por que se separo implementacion de la representacion, teniendo dos clases de implementacion y 3 de representacion o abstraccion
    public static void main(String[] args) {

        Scanner opcion = new Scanner(System.in);
        int op;
        ImplementsList implementsList;
        System.out.println("Escoga su tipo de lista");
        System.out.println("1=Repetidos , 2=Unicos");
        op=opcion.nextInt();

        switch (op){
            case 1:
                System.out.println("Creando lista, permite repetidos");
                implementsList=new RepetiveTasks();
                break;
            case 2:
                System.out.println("Creando lista, unicos");
                implementsList=new UniqueTask();
                break;
            default:
                System.out.println("Error, debes elegir una");
                System.out.println("Saliste del programa");
                return;
        }

        System.out.println("Loadingg...........");
        ListTask listTask = new ListTask();
        listTask.setImplementsList(implementsList);
        System.out.println("Lista creada");

        System.out.println("Ingresa 5 elementos");
        for(int i=0; i<5; i++){
            System.out.println("Item " + (i+1)+ "; ");
            listTask.Add(opcion.next());

       }
        System.out.println();

        System.out.println("Loadinggg, enumerada.......");
        EnumerateList enumerateList= new EnumerateList();
        enumerateList.setImplementsList(implementsList);
        System.out.println();

        System.out.println("Loading list, con viñetas..........");
        PointList pointList= new PointList();
        pointList.setImplementsList(implementsList);
        System.out.println("Diguite el simbolo: ");
        pointList.setType((char) opcion.next().charAt(0));
        System.out.println();

        System.out.println("Imprimiendo listas");

        System.out.println("Primera");
        for(int i=0; i<listTask.counter(); i++){
            System.out.println("\t" + listTask.getItem(i));

        }

        System.out.println("Segunda");
        for(int i=0; i<enumerateList.counter(); i++){
            System.out.println("\t" + enumerateList.getItem(i));
        }

        System.out.println("Tercera");
        for (int i=0; i<pointList.counter(); i++){
            System.out.println("\t" + pointList.getItem(i));
        }
    }
}