public class ListTask {
    //representacion
    protected ImplementsList implementsList;
    public void setImplementsList(ImplementsList impl){
        this.implementsList=impl;
    }
    public void Add(String item){
        implementsList.addItems(item);
    }
    public void remove(String item){
        implementsList.removeItem(item);
    }
    public int counter(){
        return implementsList.quantityItem();
    }
    public String getItem(int i){
        return implementsList.getItem(i);
    }
}
